INTRODUCTION
------------

Path Alias Force provides functionality to create forced aliases for entities
in a multilingual environment while using the language hierarchy system.
This means that whenever you create a node in a specific language and you
have other languages which fallback to it, this module will automatically
generate aliases for every other language besides the source one.


REQUIREMENTS
------------

- path
- language_hierarchy


INSTALLATION
------------

 * Install the Path Alias Force module as you would normally install
   a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

1. Go to Configuration > Search and Metadata > Generate fallback URL aliases,
   generate the fallback aliases.


MAINTAINERS
-----------

Supporting organization:

 * Axis Communications AB - https://www.drupal.org/axis-communications-ab
